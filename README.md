# CameraView

CameraView

这是一个模仿微信拍照的ohos开源控件

点击拍照

10s的视频大概1.9M左右

长按录视频（视频长度可设置）

录制完视频可以浏览并且重复播放

前后摄像头的切换

可以设置小视频保存路径

# 引用

方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码

      implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：

       allprojects{
               repositories{
                   mavenCentral()
               }
       }

       implementation 'io.openharmony.tpc.thirdlib:camera-view:1.0.5'   

# 使用：

    <com.cjt2325.cameralibrary.JCameraView
         ohos:id="$+id:jcameraview"
         ohos:height="match_parent"
         ohos:width="match_parent"
         />

权限：


    "reqPermissions": [
        {
          "name": "ohos.permission.CAMERA"
        },
        {
          "name": "ohos.permission.WRITE_USER_STORAGE"
        },
        {
          "name": "ohos.permission.MICROPHONE"
        }
      ]


全屏设置


    getWindow().setStatusBarVisibility(Component.INVISIBLE);

    "metaData":{
          "customizeData": [
            {
              "name": "hwc-theme",
              "value": "androidhwext:style/Theme.Emui.NoTitleBar",
              "extra": ""
            }
          ]
        }



初始化JCameraView控件

            getWindow().setTransparent(true);
            mJCameraView = (JCameraView) findComponentById(ResourceTable.Id_jcameraview);
            mJCameraView.setSaveVideoPath(getFilesDir() + File.separator + "JCamera");
            mJCameraView.setIconLeftAndRight(ResUtils.getVectorDrawable(getAbility(), ResourceTable.Graphic_ic_back),
                    ResUtils.getVectorDrawable(getAbility(), ResourceTable.Graphic_ic_photo));
            mJCameraView.setTip("自定义中间提醒标题");
            mJCameraView.setErrorListener((errorString, type) -> LogUtil.error(LogUtil.DEFAULT_TAG, "errorString:" + errorString));
            mJCameraView.setJCameraListener(new JCameraListener() {
                @Override
                public void captureSuccess(PixelMap bitmap) {
                    String filUrl = FileUtil.saveBitmap("JCamera", bitmap, getContext());
                    Intent fileIntent = new Intent();
                    fileIntent.setParam("PHOTO_URL", filUrl);
                    setResult(fileIntent);
                    terminate();
                }

                @Override
                public void recordSuccess(String url) {
                    LogUtil.error(LogUtil.DEFAULT_TAG, "recordSuccess");
                }
            });
            mJCameraView.setLeftClickListener(() -> {
                setResult(new Intent());
                terminate();
            });
            mJCameraView.setRightClickListener(() ->
                    LogUtil.info(LogUtil.DEFAULT_TAG, "Click Right")
            );


JCameraView生命周期:

         @Override
            public void onActive() {
                super.onActive();
                if (mJCameraView != null) {
                    mJCameraView.onResume();
                }
            }

            @Override
            public void onForeground(Intent intent) {
                super.onForeground(intent);
                if (mJCameraView != null) {
                    mJCameraView.onPause();
                }
            }


## 图片示例
<img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/cameraView.gif" width="200"/> <img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/device-2020-12-30-145724.png" width="200"/> <img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/device-2020-12-30-145844.png" width="200"/>
<img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/device-2020-12-30-145954.png" width="200"/> <img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/device-2020-12-30-151439.png" width="200"/> <img src="https://gitee.com/openharmony-tpc/CameraView/raw/master/screenshot/device-2020-12-30-151508.png" width="200"/>

## entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）