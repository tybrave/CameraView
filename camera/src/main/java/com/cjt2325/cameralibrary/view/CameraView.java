package com.cjt2325.cameralibrary.view;

import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.app.Context;
import ohos.media.image.ImageReceiver;
import ohos.media.image.PixelMap;

/**
 * Camera View
 */
public interface CameraView {
    void resetState(int type);

    void confirmState(int type);

    void showPicture(PixelMap bitmap, boolean isVertical);

    void playVideo(String url);

    void stopVideo();

    void setTip(String tip);

    void startPreviewCallback();

    boolean handlerFoucs(float x, float y);

    SurfaceProvider getSurfaceProvider();

    ImageReceiver getImageReceiver();

    Context getAppContext();
}
