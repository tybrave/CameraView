package com.cjt2325.cameralibrary.listener;

/**
 * Click Listener
 */
public interface ClickListener {
    void onClick();
}
