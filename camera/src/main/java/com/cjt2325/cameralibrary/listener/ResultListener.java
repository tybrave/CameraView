package com.cjt2325.cameralibrary.listener;

/**
 * Result Listener
 */
public interface ResultListener {
    void callback();
}
