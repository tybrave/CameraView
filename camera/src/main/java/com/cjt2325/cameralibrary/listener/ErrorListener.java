package com.cjt2325.cameralibrary.listener;

/**
 * Error Listener
 */
public interface ErrorListener {
    void onError(String errorString, int type);
}
