package com.cjt2325.cameralibrary.listener;

/**
 * Return Listener
 */
public interface ReturnListener {
    void onReturn();
}
