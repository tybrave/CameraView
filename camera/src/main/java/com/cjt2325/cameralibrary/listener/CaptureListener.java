package com.cjt2325.cameralibrary.listener;

/**
 * Capture Listener
 */
public interface CaptureListener {
    void takePictures();

    void recordShort(long time);

    void recordStart();

    void recordEnd(long time);

    void recordZoom(float zoom);

    void onError(String errorMessage, int type);
}
