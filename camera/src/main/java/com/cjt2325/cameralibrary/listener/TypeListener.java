package com.cjt2325.cameralibrary.listener;

/**
 * Type Listener
 */
public interface TypeListener {
    void cancel(boolean isFromRecord);

    void confirm(boolean isFromRecord);
}
