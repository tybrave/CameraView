package com.cjt2325.cameralibrary.listener;

import ohos.media.image.PixelMap;

/**
 * JCamera Listener
 */
public interface JCameraListener {
    void captureSuccess(PixelMap bitmap);

    void recordSuccess(String url);
}
