package com.cjt2325.cameralibrary.state;

import com.cjt2325.cameralibrary.CameraInterface;
import com.cjt2325.cameralibrary.view.CameraView;

import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Context;

/**
 * Camera Machine
 */
public class CameraMachine implements State {
    private Context mContext;
    private State mState;
    private CameraView mView;
    private CameraInterface.CameraOpenOverCallback mCameraOpenOverCallback;

    private State mPreviewState; // 浏览状态(空闲)
    private State mBorrowPictureState; // 浏览图片
    private State mBorrowVideoState; // 浏览视频

    public CameraMachine(
            Context context, CameraView view, CameraInterface.CameraOpenOverCallback cameraOpenOverCallback) {
        this.mContext = context;
        mPreviewState = new PreviewState(this);
        mBorrowPictureState = new BorrowPictureState(this);
        mBorrowVideoState = new BorrowVideoState(this);
        // 默认设置为空闲状态
        this.mState = mPreviewState;
        this.mCameraOpenOverCallback = cameraOpenOverCallback;
        this.mView = view;
    }

    /**
     * getView
     *
     * @return CameraView
     */
    public CameraView getView() {
        return mView;
    }

    /**
     * get Context
     *
     * @return Context
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * set State
     *
     * @param state State
     */
    public void setState(State state) {
        this.mState = state;
    }

    /**
     * 获取浏览图片状态
     *
     * @return State
     */
    public State getBorrowPictureState() {
        return mBorrowPictureState;
    }

    /**
     * 获取浏览视频状态
     *
     * @return State
     */
    State getBorrowVideoState() {
        return mBorrowVideoState;
    }

    /**
     * 获取空闲状态
     *
     * @return State
     */
    public State getPreviewState() {
        return mPreviewState;
    }

    @Override
    public void start(SurfaceOps holder, float screenProp) {
        mState.start(holder, screenProp);
    }

    @Override
    public void stop() {
        mState.stop();
    }

    @Override
    public void focus(float xx, float yy, CameraInterface.FocusCallback callback) {
        mState.focus(xx, yy, callback);
    }

    @Override
    public void switchCamera(SurfaceOps holder, float screenProp) {
        mState.switchCamera(holder, screenProp);
    }

    @Override
    public void restart() {
        mState.restart();
    }

    @Override
    public void capture() {
        mState.capture();
    }

    @Override
    public void record() {
        mState.record();
    }

    @Override
    public void stopRecord(boolean isShort, long time) {
        mState.stopRecord(isShort, time);
    }

    @Override
    public void cancel(SurfaceOps holder, float screenProp) {
        mState.cancel(holder, screenProp);
    }

    @Override
    public void confirm() {
        mState.confirm();
    }

    @Override
    public void zoom(float zoom, int type) {
        mState.zoom(zoom, type);
    }

    @Override
    public void flash(int mode) {
        mState.flash(mode);
    }

    /**
     * get State
     *
     * @return State
     */
    public State getState() {
        return this.mState;
    }
}
