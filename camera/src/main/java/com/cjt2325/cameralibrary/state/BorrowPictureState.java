package com.cjt2325.cameralibrary.state;

import com.cjt2325.cameralibrary.CameraInterface;
import com.cjt2325.cameralibrary.JCameraView;

import ohos.agp.graphics.SurfaceOps;

/**
 * Borrow Picture State
 */
public class BorrowPictureState implements State {
    private CameraMachine mMachine;

    public BorrowPictureState(CameraMachine machine) {
        this.mMachine = machine;
    }

    @Override
    public void start(SurfaceOps holder, float screenProp) {
        CameraInterface.getInstance().doStartPreview(holder, screenProp);
        mMachine.setState(mMachine.getPreviewState());
    }

    @Override
    public void stop() {}

    @Override
    public void focus(float x, float y, CameraInterface.FocusCallback callback) {}

    @Override
    public void switchCamera(SurfaceOps holder, float screenProp) {}

    @Override
    public void restart() {}

    @Override
    public void capture() {}

    @Override
    public void record() {}

    @Override
    public void stopRecord(boolean isShort, long time) {}

    @Override
    public void cancel(SurfaceOps holder, float screenProp) {
        CameraInterface.getInstance().doStartPreview(holder, screenProp);
        mMachine.getView().resetState(JCameraView.TYPE_PICTURE);
        mMachine.setState(mMachine.getPreviewState());
    }

    @Override
    public void confirm() {
        mMachine.getView().confirmState(JCameraView.TYPE_PICTURE);
        mMachine.setState(mMachine.getPreviewState());
    }

    @Override
    public void zoom(float zoom, int type) {}

    @Override
    public void flash(int mode) {}
}
