package com.cjt2325.cameralibrary.state;

import com.cjt2325.cameralibrary.CameraInterface;
import com.cjt2325.cameralibrary.JCameraView;
import com.cjt2325.cameralibrary.util.LogUtil;

import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;

class PreviewState implements State {
    private CameraMachine mMachine;

    PreviewState(CameraMachine machine) {
        this.mMachine = machine;
    }

    @Override
    public void start(SurfaceOps holder, float screenProp) {
        CameraInterface.getInstance().doStartPreview(holder, screenProp);
    }

    @Override
    public void stop() {
        CameraInterface.getInstance().doStopPreview();
    }

    @Override
    public void focus(float xx, float yy, CameraInterface.FocusCallback callback) {
        mMachine.getView().handlerFoucs(xx, yy);
    }

    @Override
    public void switchCamera(SurfaceOps holder, float screenProp) {
        CameraInterface.getInstance().switchCamera(holder, screenProp, mMachine);
    }

    @Override
    public void restart() {
    }

    @Override
    public void capture() {
        CameraInterface.getInstance().takePicture();
    }

    @Override
    public void record() {
        CameraInterface.getInstance().startRecord();
    }

    @Override
    public void stopRecord(final boolean isShort, long time) {
        CameraInterface.getInstance()
                .stopRecord(
                        isShort,
                        url -> {
                            if (isShort) {
                                mMachine.getView().resetState(JCameraView.TYPE_SHORT);
                            } else {
                                mMachine.getView().playVideo(url);
                                mMachine.setState(mMachine.getBorrowVideoState());
                            }
                        });
    }

    @Override
    public void cancel(SurfaceOps holder, float screenProp) {
        LogUtil.info(LogUtil.DEFAULT_TAG, "No cancel event exists in the browsing state");
    }

    @Override
    public void confirm() {
        LogUtil.info(LogUtil.DEFAULT_TAG, "No confirm event is found in the browsing state.");
    }

    @Override
    public void zoom(float zoom, int type) {
        CameraInterface.getInstance().setZoom(zoom, type);
    }

    @Override
    public void flash(int mode) {
        CameraInterface.getInstance().setFlashMode(mode);
    }
}
