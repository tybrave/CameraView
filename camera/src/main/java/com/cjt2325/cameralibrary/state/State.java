package com.cjt2325.cameralibrary.state;

import com.cjt2325.cameralibrary.CameraInterface;

import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;

/**
 * State interface
 */
public interface State {
    void start(SurfaceOps holder, float screenProp);

    void stop();

    void focus(float x, float y, CameraInterface.FocusCallback callback);

    void switchCamera(SurfaceOps holder, float screenProp);

    void restart();

    void capture();

    void record();

    void stopRecord(boolean isShort, long time);

    void cancel(SurfaceOps holder, float screenProp);

    void confirm();

    void zoom(float zoom, int type);

    void flash(int mode);
}
