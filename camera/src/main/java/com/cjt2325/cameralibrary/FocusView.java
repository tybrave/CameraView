package com.cjt2325.cameralibrary;

import com.cjt2325.cameralibrary.util.ScreenUtils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class FocusView extends Component implements Component.DrawTask {
    private int mSize;
    private int mCenter_x;
    private int mCenter_y;
    private int mLength;
    private Paint mPaint;

    public FocusView(Context context) {
        this(context, null);
    }

    public FocusView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public FocusView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mSize = ScreenUtils.getScreenWidth(context) / 3;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(new Color(0xEE16AE16));
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        addDrawTask(this);

        mCenter_x = (int) (mSize / 2.0);
        mCenter_y = (int) (mSize / 2.0);
        mLength = (int) (mSize / 2.0) - 2;
    }

    public void setX(float x) {
        setTranslationX(x - getLeft());
    }

    public void setY(float y) {
        setTranslationY(y - getTop());
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRect(
                new RectFloat(mCenter_x - mLength, mCenter_y - mLength, mCenter_x + mLength, mCenter_y + mLength),
                mPaint);
        canvas.drawLine(new Point(1, getHeight() / 2f), new Point(mSize / 10f, getHeight() / 2f), mPaint);
        canvas.drawLine(
                new Point(getWidth() - 2, getHeight() / 2f),
                new Point(getWidth() - mSize / 10f, getHeight() / 2f),
                mPaint);

        canvas.drawLine(new Point(getWidth() / 2f, 2), new Point(getWidth() / 2f, mSize / 10f), mPaint);
        canvas.drawLine(
                new Point(getWidth() / 2f, getHeight() - 2),
                new Point(getWidth() / 2f, getHeight() - mSize / 10f),
                mPaint);
    }
}
