/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjt2325.cameralibrary.constant;

/**
 * Constants
 */
public class Constants {
    /**
     * ERROR TYPE_PICTURE
     */
    public static final int ERROR_TYPE_PICTURE = 0x00101;

    /**
     * ERROR TYPE_RECORD
     */
    public static final int ERROR_TYPE_RECORD = 0x00102;
    /**
     * recorder fps
     */
    public static final int RECORDER_FPS = 30;

    /**
     * recorder rate
     */
    public static final int RECORDER_RATE = 30;

    private Constants() {
    }
}
