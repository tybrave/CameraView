package com.cjt2325.cameralibrary.util;

import ohos.system.DeviceInfo;

import java.util.Arrays;

/**
 * Device Util
 */
public class DeviceUtil {
    private static String[] sHuaweiRongyao = {
            "hwH60", // 荣耀6
            "hwPE", // 荣耀6 plus
            "hwH30", // 3c
            "hwHol", // 3c畅玩版
            "hwG750", // 3x
            "hw7D", // x1
            "hwChe2", // x1
    };

    /**
     * get Device Model
     *
     * @return String
     */
    public static String getDeviceModel() {
        return DeviceInfo.getDeviceType();
    }

    /**
     * isHuaWei Rongyao
     *
     * @return boolean
     */
    public static boolean isHuaWeiRongyao() {
        int length = sHuaweiRongyao.length;
        for (int i = 0; i < length; i++) {
            if (sHuaweiRongyao[i].equals(getDeviceModel())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 数组合并
     *
     * @param first  T[]
     * @param second T[]
     * @param <T>    T
     * @return <T> T[]
     */
    public static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
