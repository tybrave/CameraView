package com.cjt2325.cameralibrary.util;

/**
 * Angle Util
 */
public class AngleUtil {
    /**
     * get Sensor Angle
     * @param rx float
     * @param ry float
     * @return int
     */
    public static int getSensorAngle(float rx, float ry) {
        if (Math.abs(rx) > Math.abs(ry)) {
            /**
             * 横屏倾斜角度比较大
             */
            if (rx > 4) {
                /**
                 * 左边倾斜
                 */
                return 270;
            } else if (rx < -4) {
                /**
                 * 右边倾斜
                 */
                return 90;
            } else {
                /**
                 * 倾斜角度不够大
                 */
                return 0;
            }
        } else {
            if (ry > 7) {
                /**
                 * 左边倾斜
                 */
                return 0;
            } else if (ry < -7) {
                /**
                 * 右边倾斜
                 */
                return 180;
            } else {
                /**
                 * 倾斜角度不够大
                 */
                return 0;
            }
        }
    }
}
