package com.cjt2325.cameralibrary.util;

import ohos.app.Context;
import ohos.media.audio.AudioManager;

/**
 * Audio Util
 */
public class AudioUtil {
    /**
     * set AudioManage
     * @param context Context
     */
    public static void setAudioManage(Context context) {
        AudioManager audioManager = new AudioManager(context);

        audioManager.mute(AudioManager.AudioVolumeType.STREAM_SYSTEM);
        audioManager.mute(AudioManager.AudioVolumeType.STREAM_MUSIC);

        audioManager.setVolume(AudioManager.AudioVolumeType.STREAM_ALARM, 0);
        audioManager.setVolume(AudioManager.AudioVolumeType.STREAM_DTMF, 0);
        audioManager.setVolume(AudioManager.AudioVolumeType.STREAM_NOTIFICATION, 0);
        audioManager.setVolume(AudioManager.AudioVolumeType.STREAM_RING, 0);
    }
}
