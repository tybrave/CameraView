package com.cjt2325.cameralibrary;

import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Return Button
 */
public class ReturnButton extends Component implements Component.DrawTask {
    private int mSize;

    private int mCenter_X;
    private int mCenter_Y;
    private float mStrokeWidth;

    private Paint mPaint;
    private Path mPath;

    public ReturnButton(Context context, int size) {
        this(context);
        this.mSize = size;
        mCenter_X = size / 2;
        mCenter_Y = size / 2;

        mStrokeWidth = size / 15f;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mStrokeWidth);

        mPath = new Path();
        addDrawTask(this);
    }

    public ReturnButton(Context context) {
        super(context);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPath.moveTo(mStrokeWidth, mStrokeWidth / 2);
        mPath.lineTo(mCenter_X, mCenter_Y - mStrokeWidth / 2);
        mPath.lineTo(mSize - mStrokeWidth, mStrokeWidth / 2);
        canvas.drawPath(mPath, mPaint);
    }
}
